<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>{{  __('content.about')  }}</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap"
            rel="stylesheet"
        />
        <link rel="stylesheet" href="css/style.min.css" />
    </head>
    <body>
        <section class="main main2">
            <div class="container">
                <header class="header">
                    <a class="logo" href="/">
                        <img src="img/logo.svg" alt="" />
                    </a>
                    <div class="header__links">
                        <a class="header__link" href="/about">{{  __('content.about')  }}</a>
                        <a class="header__link" href="/services">{{  __('content.services')  }}</a>
                        <a class="header__link" href="/articles">{{  __('content.articles')  }}</a>
                        <a class="header__link" href="/procurements">{{  __('content.procurement')  }}</a>
                        <a class="header__link" href="/news">{{  __('content.news')  }}</a>
                    </div>
                    <div class="header__setting">
                        <a class="header__tel" href="tel:77751231875"
                            >{{ $contact->number }}</a
                        >
                        <div style="display: flex" class="languages">
                            <a class="language" href="/setlocale/ru">ru</a><a style="margin-left: 10px" class="language" href="/setlocale/en">en</a><a style="margin-left: 10px" class="language" href="/setlocale/kz">kz</a>
                        </div>
                    </div>
                </header>
                <div class="company">
                    <div class="breadcrumb">
                        <a href="">{{  __('content.main')  }}</a>
                        <a class="breadcrumb__sub" href="">{{  __('content.about')  }}</a>
                    </div>
                    <div class="list__title">{{ $about->title }}</div>
                    <div class="company__wrap">
                        <div class="company__img">
                            <img src="/storage/{{ $about->image }}" alt="" />
                        </div>
                        <div class="company__txt">
                            <p>
                                {!! $about->description !!}
                            </p>
                        </div>
                    </div>
                    <div class="quote">
                        <div class="quote__title">{{ $mission->title }}</div>
                        <p>
                            {{ $mission->description }}
                        </p>
                    </div>
                    <div class="decision">
                        <div class="decision__center">
                            <div class="title">{{  __('content.answears')  }}</div>
                        </div>
                        <div class="decision__items">
                            @foreach ($answears as $answear)    
                                <div class="decision__item">
                                    <div class="decision__img">
                                        <img src="/storage/{{ $answear->icon }}" alt="" />
                                    </div>
                                    <div class="decision__title">
                                        {{ $answear->title }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="technology">
                        <div class="technology__descr">
                            <div class="title">{{ $technology->title }}</div>
                            <div class="subtitle">
                                {{ $technology->description }}
                            </div>
                        </div>
                        <div class="technology__items">
                            @foreach ($icons as $icon)
                                <div class="technology__item">
                                    <div class="technology__item-img">
                                        <img src="/storage/{{ $icon->icon }}" alt="" />
                                    </div>
                                    <div class="technology__item-title">
                                        {{ $icon->title }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="partners">
                        <div class="decision__center">
                            <div class="title">{{  __('content.partners')  }}</div>
                        </div>
                        <div class="partners__items">
                            @foreach ($partners as $partner)
                                <div class="partners__item">
                                    <img src="/storage/{{ $partner->image }}" alt="" />
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <img class="main-bg2" src="img/main-bg2.svg" alt="" />
        </section>
        <footer class="footer">
            <div class="container">
                <div class="footer__inner">
                    <div class="footer__logo">
                        <img src="img/logo-footer.svg" alt="" />
                    </div>
                    <div class="footer__flex">
                        <div class="footer__links">
                            <a class="footer__link" href="">{{  __('content.about')  }}</a>
                            <a class="footer__link" href="">{{  __('content.services')  }}</a>
                            <a class="footer__link" href="">{{  __('content.procurement')  }}</a>
                        </div>
                        <div class="footer__links">
                            <a class="footer__link" href="">{{  __('content.news')  }}</a>
                            <a class="footer__link" href="">{{  __('content.articles')  }}</a>
                        </div>
                    </div>
                    <div class="footer__links footer__n">
                        <div class="footer__one">{{  __('content.adress')  }}:</div>
                        <div class="footer__two">
                            {{ $contact->address }}
                        </div>
                    </div>
                    <div class="footer__contacts">
                        <div>
                            <img src="img/msq.svg" alt="" />
                            {{ $contact->email }}
                        </div>
                        <div>
                            <img src="img/Iconly/Light/Call.svg" alt="" />
                            {{ $contact->number }}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="js/libs.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
