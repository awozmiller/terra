<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>{{  __('content.main')  }}</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap"
            rel="stylesheet"
        />
        <link rel="stylesheet" href="css/style.min.css" />
    </head>
    <body>
        <section class="main">
            <div class="container">
                <header class="header">
                    <a class="logo" href="/">
                        <img src="img/logo.svg" alt="" />
                    </a>
                    <div class="header__links">
                        <a class="header__link" href="/about">{{  __('content.about')  }}</a>
                        <a class="header__link" href="/services">{{  __('content.services')  }}</a>
                        <a class="header__link" href="/articles">{{  __('content.articles')  }}</a>
                        <a class="header__link" href="/procurements">{{  __('content.procurement')  }}</a>
                        <a class="header__link" href="/news">{{  __('content.news')  }}</a>
                    </div>
                    <div class="header__setting">
                        <a class="header__tel" href="tel:77751231875"
                            >{{ $contact->number }}</a
                        >
                        <div style="display: flex" class="languages">
                            <a class="language" href="/setlocale/ru">ru</a><a style="margin-left: 10px" class="language" href="/setlocale/en">en</a><a style="margin-left: 10px" class="language" href="/setlocale/kz">kz</a>
                        </div>
                    </div>
                </header>
                <div class="main__inner">
                    @foreach ($banners as $banner)
                        <div>
                            <div class="main__slider">
                                <div class="main__descr">
                                    <div class="main__title">
                                        {{ $banner->title }}
                                    </div>
                                    <div class="main__subtitle">
                                        {{ $banner->description }}
                                    </div>
                                    <button class="button">{{  __('content.button')  }}</button>
                                </div>
                                <div class="main__img">
                                    <div class="main__img-wrap">
                                        <img src="/storage/{{ $banner->image }}" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <img class="main-bg2" src="img/main-bg2.svg" alt="" />
        </section>
        <section class="about">
            <div class="container">
                <div class="about__inner">
                    <div class="title">{{ $about->title }}</div>
                    <div class="subtitle">
                        <p>
                            {!! $about->description !!}
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="advert">
            <div class="container">
                <div class="advert__inner">
                    <div class="advert__items">
                        <div class="advert__item">
                            <div class="title">{{  __('content.advantages')  }}</div>
                        </div>
                        @foreach ($advantages as $advantage)
                            <div class="advert__item">
                                <div class="advert__item-img">
                                    <img src="/storage/{{ $advantage->icon }}" alt="" />
                                </div>
                                <div class="advert__item-title">{{ $advantage->title }}</div>
                                <div class="advert__item-subtitle">
                                    {{ $advantage->description }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <section class="serv">
            <img class="serv-bg" src="img/serv-main.svg" alt="" />
            <div class="container">
                <div class="serv__inner">
                    <div class="serv__center">
                        <div class="title">{{  __('content.services')  }}</div>
                    </div>
                    <div class="technology">
                        <div class="technology__descr">
                            <div class="title">{{ $service->title }}</div>
                            <div class="subtitle">
                                {{ $service->description }}
                            </div>
                        </div>
                        <div class="technology__items">
                           @foreach ($technologies as $technology)
                                <div class="technology__item">
                                    <div class="technology__item-img">
                                    <img src="/storage/{{ $technology->icon }}" alt="" />
                                    </div>
                                    <div class="technology__item-title">
                                        {{ $technology->title }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="technology technology2">
                        <div class="technology__items">
                            @foreach ($tech_icons as $tech_icon)
                                <div class="technology__item">
                                    <div class="technology__item-img">
                                        <img src="/storage/{{ $tech_icon->icon }}" alt="" />
                                    </div>
                                    <div class="technology__item-title">
                                        {{ $tech_icon->title }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="technology__descr">
                            <div class="title">{{ $technical->title }}</div>
                            <div class="subtitle">
                                {{ $technical->description }}
                            </div>
                        </div>
                    </div>
                    <div class="technology">
                        <div class="technology__descr">
                            <div class="title">{{ $system->title }}</div>
                            <div class="subtitle">
                                {{ $system->description }}
                            </div>
                        </div>
                        <div class="technology__items">
                            @foreach ($system_icons as $system_icon)
                                <div class="technology__item">
                                    <div class="technology__item-img">
                                        <img src="/storage/{{ $system_icon->icon }}" alt="" />
                                    </div>
                                    <div class="technology__item-title">
                                        {{ $system_icon->title }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="news-main">
            <div class="container">
                <div class="serv__center">
                    <div class="title">{{  __('content.news')  }}</div>
                </div>
                <div class="list__items">
                    @foreach ($news as $news)
                    <div class="list__item">
                        <div class="list__item-img">
                            <img src="/storage/{{ $news->image }}" alt="" />
                        </div>
                        <div class="list__item-date">{{ $news->date }}</div>
                        <div class="list__item-title">
                            {{ $news->title }}
                        </div>
                        <div class="list__item-subtitle">
                            {!! $news->description !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
        <section class="zakup">
            <img class="bg-zakup" src="img/bg-zakup.svg" alt="" />
            <div class="container">
                <div class="zakup__inner">
                    <div class="serv__center">
                        <div class="title">{{  __('content.procurement')  }}</div>
                    </div>
                    <div class="list__items">
                        @foreach ($procurements as $procurement)
                            <div class="list__item">
                                <div class="zakup__date">{{ $procurement->date }}</div>
                                <div class="zakup__title">
                                    {{ $procurement->title }}
                                </div>
                                <a href="" class="zakup__more">{{  __('content.button')  }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
        <footer class="footer">
            <div class="container">
                <div class="footer__inner">
                    <div class="footer__logo">
                        <img src="img/logo-footer.svg" alt="" />
                    </div>
                    <div class="footer__flex">
                        <div class="footer__links">
                            <a class="footer__link" href="">{{  __('content.about')  }}</a>
                            <a class="footer__link" href="">{{  __('content.services')  }}</a>
                            <a class="footer__link" href="">{{  __('content.procurement')  }}</a>
                        </div>
                        <div class="footer__links">
                            <a class="footer__link" href="">{{  __('content.news')  }}</a>
                            <a class="footer__link" href="">{{  __('content.articles')  }}</a>
                        </div>
                    </div>
                    <div class="footer__links footer__n">
                        <div class="footer__one">{{  __('content.adress')  }}:</div>
                        <div class="footer__two">
                            {{ $contact->address }}
                        </div>
                    </div>
                    <div class="footer__contacts">
                        <div>
                            <img src="img/msq.svg" alt="" />
                            {{ $contact->email }}
                        </div>
                        <div>
                            <img src="img/Iconly/Light/Call.svg" alt="" />
                            {{ $contact->number }}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="js/libs.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
