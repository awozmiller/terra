<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>{{  __('content.procurement')  }}</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap"
            rel="stylesheet"
        />
        <link rel="stylesheet" href="css/style.min.css" />
    </head>
    <body>
        <section class="main">
            <div class="container">
                <header class="header">
                    <a class="logo" href="/">
                        <img src="img/logo.svg" alt="">
                    </a>
                    <div class="header__links">
                        <a class="header__link" href="/about">{{  __('content.about')  }}</a>
                        <a class="header__link" href="/services">{{  __('content.services')  }}</a>
                        <a class="header__link" href="/articles">{{  __('content.articles')  }}</a>
                        <a class="header__link" href="/procurements">{{  __('content.procurement')  }}</a>
                        <a class="header__link" href="/news">{{  __('content.news')  }}</a>
                    </div>
                    <div class="header__setting">
                        <a class="header__tel" href="tel:77751231875">{{ $contact->number }}</a>
                        <div style="display: flex" class="languages">
                            <a class="language" href="/setlocale/ru">ru</a><a style="margin-left: 10px" class="language" href="/setlocale/en">en</a><a style="margin-left: 10px" class="language" href="/setlocale/kz">kz</a>
                        </div>
                    </div>
                </header>
                <div class="list__inner">
                    <div class="breadcrumb">
                        <a href=""></a>
                        <a class="breadcrumb__sub" href="">{{  __('content.procurement')  }}</a>
                    </div>
                    <div class="list__title">{{  __('content.procurement')  }}</div>
                    <div class="checklist__items">
                        @foreach ($procurements as $procurement)
                            <div class="checklist__item">
                                <div class="checklist__item-date">{{ $procurement->date }}</div>
                                <a href="" class="checklist__item-link">{{ $procurement->title }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <img class="main-bg2" src="img/main-bg2.svg" alt="">
        </section>
        <header class="hader"></header>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="js/libs.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
