<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>{{  __('content.articles')  }}</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap"
            rel="stylesheet"
        />
        <link rel="stylesheet" href="css/style.min.css" />
    </head>
    <body>
        <section class="main">
            <div class="container">
                <header class="header">
                    <a class="logo" href="/">
                        <img src="img/logo.svg" alt="">
                    </a>
                    <div class="header__links">
                        <a class="header__link" href="/about">{{  __('content.about')  }}</a>
                        <a class="header__link" href="/services">{{  __('content.services')  }}</a>
                        <a class="header__link" href="/articles">{{  __('content.articles')  }}</a>
                        <a class="header__link" href="/procurements">{{  __('content.procurement')  }}</a>
                        <a class="header__link" href="/news">{{  __('content.news')  }}</a>
                    </div>
                    <div class="header__setting">
                        <a class="header__tel" href="tel:77751231875">{{ $contact->number }}</a>
                        <div style="display: flex" class="languages">
                            <a class="language" href="/setlocale/ru">ru</a><a style="margin-left: 10px" class="language" href="/setlocale/en">en</a><a style="margin-left: 10px" class="language" href="/setlocale/kz">kz</a>
                        </div>
                    </div>
                </header>
                <div class="list__inner">
                    <div class="breadcrumb">
                        <a href="/">{{  __('content.main')  }}</a>
                        <a class="breadcrumb__sub" href="">{{  __('content.articles')  }}</a>
                    </div>
                    <div class="list__title">{{  __('content.articles')  }}</div>
                    <div class="list__items">
                        @foreach ($articles as $article)
                            <div class="list__item">
                                <div class="list__item-img">
                                    <img src="/storage/{{ $article->image }}" alt="">
                                </div>
                                <div class="list__item-date">{{ $article->date }}</div>
                                <div class="list__item-title"><a href="{{route('getArticle',[$article->id])}}">{{ $article->title }}</a></div>
                                <div class="list__item-subtitle">{!! $article->description !!}</div>
                            </div>
                        @endforeach
                    </div>
                    <ul class="pagination">
                        <li class="pagination-left">
                            <svg width="7" height="11" viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.39587 1.70832L1.60421 5.49999L5.39587 9.29166" stroke="#3E413C" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </li>
                        <li class="active">1</li>
                        <li>2</li>
                        <li>3</li>
                        <li class="pagination-right">
                            <svg width="7" height="11" viewBox="0 0 7 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.60413 1.70832L5.39579 5.49999L1.60413 9.29166" stroke="#3E413C" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </li>
                    </ul>
                </div>
            </div>
            <img class="main-bg2" src="img/main-bg2.svg" alt="">
        </section>
        <header class="hader"></header>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="js/libs.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
