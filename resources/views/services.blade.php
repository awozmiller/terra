<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>{{  __('content.services')  }}</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap"
            rel="stylesheet"
        />
        <link rel="stylesheet" href="css/style.min.css" />
    </head>
    <body>
        <section class="main main2">
            <div class="container">
                <header class="header">
                    <a class="logo" href="/">
                        <img src="img/logo.svg" alt="" />
                    </a>
                    <div class="header__links">
                        <a class="header__link" href="/about">{{  __('content.about')  }}</a>
                        <a class="header__link" href="/services">{{  __('content.services')  }}</a>
                        <a class="header__link" href="/articles">{{  __('content.articles')  }}</a>
                        <a class="header__link" href="/procurements">{{  __('content.procurement')  }}</a>
                        <a class="header__link" href="/news">{{  __('content.news')  }}</a>
                    </div>
                    <div class="header__setting">
                        <a class="header__tel" href="tel:77751231875"
                            >{{ $contact->number }}</a
                        >
                        <div style="display: flex" class="languages">
                            <a class="language" href="/setlocale/ru">ru</a><a style="margin-left: 10px" class="language" href="/setlocale/en">en</a><a style="margin-left: 10px" class="language" href="/setlocale/kz">kz</a>
                        </div>
                    </div>
                </header>
                <div class="list__inner">
                    <div class="breadcrumb">
                        <a href="">{{  __('content.main')  }}</a>
                        <a class="breadcrumb__sub" href="">{{  __('content.services')  }}</a>
                    </div>
                    <div class="list__title">{{  __('content.services')  }}</div>
                    <div class="services__inner">
                        <div class="services__wrap">
                            <div class="title">{{ $t_text->title }}</div>
                            <div class="services__suptitle">
                                {{ $t_text->description}}
                            </div>
                        </div>
                        <div class="list__items services__items">
                            @foreach ($telecommunications as $telecommuinication)
                                <div class="list__item">
                                    <div class="list__item-img">
                                        <img src="/storage/{{ $telecommuinication->image }}" alt="" />
                                    </div>
                                    <div class="services__item-img">
                                        <img src="/storage/{{ $telecommuinication->icon }}" alt="" />
                                    </div>
                                    <div class="list__item-title">
                                        {{ $telecommuinication->title }}
                                    </div>  
                                    <div class="list__item-subtitle">
                                        {{ $telecommuinication->description }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="services__inner services__inner2">
                        <div class="services__wrap">
                            <div class="title">{{ $m_text->title }}</div>
                            <div class="services__suptitle">
                                {{ $m_text->description }}
                            </div>
                        </div>
                        <div class="list__items services__items">
                            @foreach ($maintenances as $maintenance)
                                <div class="list__item">
                                    <div class="list__item-img">
                                        <img src="/storage/{{ $maintenance->image }}" alt="" />
                                    </div>
                                    <div class="services__item-img">
                                        <img src="/storage/{{ $maintenance->icon }}" alt="" />
                                    </div>
                                    <div class="list__item-title">
                                        {{ $maintenance->title }}
                                    </div>  
                                    <div class="list__item-subtitle">
                                        {{ $maintenance->description }}
                                    </div>
                                </div>
                            @endforeach
                    </div>
                    <div class="services__inner services__inner2">
                        <div class="services__wrap">
                            <div class="title">{{ $s_text->title }}</div>
                            <div class="services__suptitle">
                                {{ $s_text->description }}
                            </div>
                        </div>
                        <div class="list__items services__items">
                            @foreach ($integrations as $integration)
                                <div class="list__item">
                                    <div class="list__item-img">
                                        <img src="/storage/{{ $integration->image }}" alt="" />
                                    </div>
                                    <div class="services__item-img">
                                        <img src="/storage/{{ $integration->icon }}" alt="" />
                                    </div>
                                    <div class="list__item-title">
                                        {{ $integration->title }}
                                    </div>  
                                    <div class="list__item-subtitle">
                                        {{ $integration->description }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <img class="main-bg2" src="img/main-bg2.svg" alt="" />
        </section>
        <footer class="footer">
            <div class="container">
                <div class="footer__inner">
                    <div class="footer__logo">
                        <img src="img/logo-footer.svg" alt="" />
                    </div>
                    <div class="footer__flex">
                        <div class="footer__links">
                            <a class="footer__link" href="">{{  __('content.about')  }}</a>
                            <a class="footer__link" href="">{{  __('content.services')  }}</a>
                            <a class="footer__link" href="">{{  __('content.procurement')  }}</a>
                        </div>
                        <div class="footer__links">
                            <a class="footer__link" href="">{{  __('content.news')  }}</a>
                            <a class="footer__link" href="">{{  __('content.articles')  }}</a>
                        </div>
                    </div>
                    <div class="footer__links footer__n">
                        <div class="footer__one">{{  __('content.adress')  }}:</div>
                        <div class="footer__two">
                            {{ $contact->address }}
                        </div>
                    </div>
                    <div class="footer__contacts">
                        <div>
                            <img src="img/msq.svg" alt="" />
                            {{ $contact->email }}
                        </div>
                        <div>
                            <img src="img/Iconly/Light/Call.svg" alt="" />
                            {{ $contact->number }}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="js/libs.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
