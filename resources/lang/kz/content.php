<?php

return [
    'about' => 'Біз туралы',
    'services' => 'Қызметтер',
    'procurement' => 'Сатып алу',
    'articles' => 'Мақалалар',
    'button' => 'Толығырақ',
    'adress' => 'Мекенжай',
    'news' => 'Жаңалықтар',
    'advantages'=>'Біздің артықшылығымыз',
    'main' => 'Уй',
    'partners'=>'Серіктестер',
    'answears'=>'Тиімді Шешімдер'
];
