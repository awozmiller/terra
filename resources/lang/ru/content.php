<?php

return [
    'about' => 'О нас',
    'services' => 'Услуги',
    'procurement' => 'Закупки',
    'articles' => 'Статьи',
    'button' => 'Узнать больше',
	'buttons' => 'Подробнее',
    'adress' => 'Адрес',
    'news' => 'Новости',
    'main' => 'Главная',
    'partners'=>'Партнеры',
    'advantages'=>'Наши Преимущество',
    'answears'=>'Эффективные Решения'
];
