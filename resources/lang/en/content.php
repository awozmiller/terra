<?php

return [
    'about' => 'About',
    'services' => 'Services',
    'procurement' => 'Procurements',
    'articles' => 'Articles',
    'button' => 'More',
	'buttons' => 'More Details',
    'adress' => 'Address',
    'news' => 'News',
    'main' => 'Main',
    'partners'=>'Partners',
    'advantages'=>'Our Advantages',
    'answears'=>'Effective Solutions'
];
