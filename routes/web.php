<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('/about', 'MainController@about');
Route::get('/services', 'MainController@service');
Route::get('/news', 'MainController@news');
Route::get('/news/{id}', 'MainController@getNews')->name('getNews');
Route::get('/procurements', 'MainController@procurements');
Route::get('/articles', 'MainController@articles');
Route::get('/article/{id}', 'MainController@getArticle')->name('getArticle');
Route::get('/setlocale/{locale}', function ($locale) {
    session(['locale' => $locale]);
    return redirect()->back();
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
