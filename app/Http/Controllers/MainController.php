<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\About;
use App\Models\Advantage;
use App\Models\News;
use App\Models\Procurement;
use App\Models\Contact;
use App\Models\MainAbout;
use App\Models\MissionAbout;
use App\Models\AboutAnswear;
use App\Models\AboutTech;
use App\Models\Partner;
use App\Models\Telecommunication;
use App\Models\Maintenance;
use App\Models\Integration;
use App\Models\Article;
Use App\Models\Service;
Use App\Models\Technology;
Use App\Models\Technical;
Use App\Models\TechnicalIcon;
Use App\Models\System;
Use App\Models\SystemIcon;
Use App\Models\Tele_Text;
Use App\Models\System_Text;
Use App\Models\Main_Text;
Use App\Models\Icon;
Use App\Models\ServiceBadge;



class MainController extends Controller
{
    public function index(){
        if(session()->has('locale')) {

            $locale = session('locale');
            App::setLocale($locale);
    }
    else {
        $locale = session(['locale' => 'ru']);
        App::setLocale('ru');
    }


        $banners = Banner::all()->translate($locale, 'ru');
        $about = About::first()->translate($locale, 'ru');    
        $advantages = Advantage::all()->translate($locale, 'ru');   
        $service = Service::first()->translate($locale, 'ru');
        $technologies = Technology::all()->translate($locale, 'ru');
        $technical = Technical::first()->translate($locale, 'ru');
        $tech_icons = TechnicalIcon::all()->translate($locale, 'ru');
        $system = System::first()->translate($locale, 'ru');
        $system_icons = SystemIcon::all()->translate($locale, 'ru');
        $news = News::all()->translate($locale, 'ru'); 
        $procurements = Procurement::all()->translate($locale, 'ru');
        $contact = Contact::first()->translate($locale, 'ru');
        return view('index',compact('banners','about','advantages','news','procurements','contact','service','technologies','technical','tech_icons','system','system_icons'));
    }

    public function about(){
        if(session()->has('locale')) {

            $locale = session('locale');
            App::setLocale($locale);
    }
    else {
        $locale = session(['locale' => 'ru']);
        App::setLocale('ru');
    }


        $about = MainAbout::first()->translate($locale, 'ru');
        $mission = MissionAbout::first()->translate($locale, 'ru');
        $answears = AboutAnswear::all()->translate($locale, 'ru');
        $technology = AboutTech::first()->translate($locale, 'ru');
        $icons = ServiceBadge::all()->translate($locale, 'ru');
        $partners = Partner::all();
        $contact = Contact::first()->translate($locale, 'ru');
        return view('company',compact('about','mission','answears','technology','partners','contact','icons'));
    }

    public function service(){
        if(session()->has('locale')) {

            $locale = session('locale');
            App::setLocale($locale);
    }
    else {
        $locale = session(['locale' => 'ru']);
        App::setLocale('ru');
    }


        $telecommunications = Telecommunication::all()->translate($locale, 'ru');
        $maintenances = Maintenance::all()->translate($locale, 'ru');
        $integrations = Integration::all()->translate($locale, 'ru');
        $contact = Contact::first()->translate($locale, 'ru');
        $t_text = Tele_Text::first()->translate($locale, 'ru');
        $m_text = Main_Text::first()->translate($locale, 'ru');
        $s_text = System_Text::first()->translate($locale, 'ru');
        $contact = Contact::first()->translate($locale, 'ru');
        return view('services',compact('telecommunications','maintenances','integrations','contact','t_text','m_text','s_text'));
    }

    public function news(){
        if(session()->has('locale')) {

            $locale = session('locale');
            App::setLocale($locale);
    }
    else {
        $locale = session(['locale' => 'ru']);
        App::setLocale('ru');
    }
        $contact = Contact::first()->translate($locale, 'ru');
        $news = News::all()->translate($locale, 'ru');
        return view('list',compact('news','contact'));
    }

    public function getNews($id){
        if(session()->has('locale')) {

            $locale = session('locale');
            App::setLocale($locale);
    }
    else {
        $locale = session(['locale' => 'ru']);
        App::setLocale('ru');
    }

        $contact = Contact::first()->translate($locale, 'ru');
        $news = News::find($id)->translate($locale, 'ru');
        return view('list-item',compact('news','contact'));
    }

    public function procurements(){
        if(session()->has('locale')) {

            $locale = session('locale');
            App::setLocale($locale);
    }
    else {
        $locale = session(['locale' => 'ru']);
        App::setLocale('ru');
    }

        $contact = Contact::first()->translate($locale, 'ru');
        $procurements = Procurement::all()->translate($locale, 'ru');
        return view('checklist',compact('procurements','contact'));
    }

    public function articles(){
        if(session()->has('locale')) {

            $locale = session('locale');
            App::setLocale($locale);
    }
    else {
        $locale = session(['locale' => 'ru']);
        App::setLocale('ru');
    }
     
        $contact = Contact::first()->translate($locale, 'ru');
        $articles = Article::all()->translate($locale, 'ru');
        return view('articles   ',compact('articles','contact'));
    }

    public function getArticle($id){
        if(session()->has('locale')) {

            $locale = session('locale');
            App::setLocale($locale);
    }
    else {
        $locale = session(['locale' => 'ru']);
        App::setLocale('ru');
    }

        $contact = Contact::first()->translate($locale, 'ru');
        $article = Article::find($id)->translate($locale, 'ru');
        return view('article-item',compact('article','contact'));
    }
}
